<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCaringRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nurse_id' => ['exists:nurses,id'],
            'description' => ['string'],
            'patient_id' => ['exists:patients,id'],
            'type_id' => ['exists:caring_types,id'],
            'time' => ['date']
        ];
    }
}
