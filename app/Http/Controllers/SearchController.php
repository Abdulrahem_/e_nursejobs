<?php

namespace App\Http\Controllers;

use App\Models\Caring;
use App\Models\CaringType;
use App\Models\Nurse;
use App\Models\Patient;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request) {
        $nurses = Nurse::query()->where('name', 'LIKE', '%'. $request->name . '%')->get();
        $patients = Patient::query()->where('name', 'LIKE', '%'. $request->name . '%')->get();
        $caringTypes = CaringType::query()->where('name', 'LIKE', '%'. $request->name . '%')->get();
        $caring = Caring::query()->where('time', $request->date )->get();

        $data['nurses'] = $nurses;
        $data['patients'] = $patients;
        $data['caringTypes'] = $caringTypes;
        $data['caring'] = $caring;
        return successResponse($data);
    }

}
