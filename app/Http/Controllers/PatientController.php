<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use App\Http\Requests\StorePatientRequest;
use App\Http\Requests\UpdatePatientRequest;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function index()
    {
        $patients = Patient::query()->get();
        return successResponse($patients);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePatientRequest $request)
    {
        $data = $request->except('photo');
        if ($request->hasFile('photo')) {
            $data['photo'] = uploadFile('patients/images', $request->photo);
        }
        $patient = Patient::query()->create($data);
        return successResponse($patient);
    }

    /**
     * Display the specified resource.
     */
    public function show(Patient $patient)
    {
        return successResponse($patient);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePatientRequest $request, Patient $patient)
    {
        $data = $request->except('photo');
        if ($request->hasFile('photo')) {
            $data['photo'] = uploadFile('patients/images', $request->photo);
        }
        $patient->update($data);
        return successResponse($patient);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();
        return successMessage('deleted');
    }
}
