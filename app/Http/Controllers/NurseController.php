<?php

namespace App\Http\Controllers;

use App\Models\Nurse;
use App\Http\Requests\StoreNurseRequest;
use App\Http\Requests\UpdateNurseRequest;
use Illuminate\Support\Facades\Hash;

class NurseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $nurses = Nurse::query()->get();
        return successResponse($nurses);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreNurseRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make(123456);
        $nurse = Nurse::query()->create($data);
        $nurse->refresh();
        return successResponse($nurse);
    }

    /**
     * Display the specified resource.
     */
    public function show(Nurse $nurse)
    {
        return successResponse($nurse);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateNurseRequest $request, Nurse $nurse)
    {
        $nurse->update($request->all());
        return successResponse($nurse);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Nurse $nurse)
    {
        $nurse->delete();
        return successMessage('deleted');
    }

    public function myPatients()
    {
        $nurse = auth()->user();
        $data = $nurse->patients;
        return successResponse($data);
    }
}
