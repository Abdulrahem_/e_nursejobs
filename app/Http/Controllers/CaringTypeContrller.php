<?php

namespace App\Http\Controllers;

use App\Models\CaringType;
use Illuminate\Http\Request;

class CaringTypeContrller extends Controller
{
    public function index()
    {
        $caringTypes = CaringType::query()->get();
        return successResponse($caringTypes);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'description' => ['required', 'string']
        ]);
        $caringType = CaringType::query()->create($request->all());
        return successResponse($caringType);
    }

    /**
     * Display the specified resource.
     */
    public function show(CaringType $caringType)
    {
        return successResponse($caringType);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CaringType $caringType)
    {
        $request->validate([
            'name' => ['string'],
            'description' => ['string']
        ]);
        $caringType->update($request->all());
        return successResponse($caringType);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CaringType $caringType)
    {
        $caringType->delete();
        return successMessage('deleted');
    }
}
