<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Admin;
use App\Models\Nurse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
     public function nurseLogin(LoginRequest $request)
    {
        $nurse = Nurse::query()
        ->where('phone', $request->phone)
        ->first();
        if (!isset($nurse)) {
            return errorResponse('nurse not found', 404);
        }
        $check = Hash::check($request->password, $nurse->password);
        if (! $check) {
            return errorResponse('incorect password', 403);
        }
        $token = $nurse->createToken('mobile', ['role:nurse']);
        $data['nurse'] = $nurse;
        $data['type'] = 'Bearer';
        $data['token'] = $token->plainTextToken;

        return successResponse($data);
    }
    public function adminLogin(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'min:6']
        ]);

        $admin = Admin::query()
        ->where('email', $request->email)
        ->first();
        if (!isset($admin)) {
            return errorResponse('admin not found', 404);
        }
        $check = Hash::check($request->password, $admin->password);
        if (! $check) {
            return errorResponse('incorect password', 403);
        }
        $token = $admin->createToken('mobile', ['role:admin']);
        $data['admin'] = $admin;
        $data['type'] = 'Bearer';
        $data['token'] = $token->plainTextToken;

        return successResponse($data);
    }
}
