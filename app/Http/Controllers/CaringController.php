<?php

namespace App\Http\Controllers;

use App\Models\Caring;
use App\Http\Requests\StoreCaringRequest;
use App\Http\Requests\UpdateCaringRequest;

class CaringController extends Controller
{
    public function index()
    {
        $caring = Caring::query()->get();
        return successResponse($caring);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCaringRequest $request)
    {
        $caring = Caring::query()->create($request->all());
        return successResponse($caring);
    }

    /**
     * Display the specified resource.
     */
    public function show(Caring $caring)
    {
        return successResponse($caring);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCaringRequest $request, Caring $caring)
    {
        $caring->update($request->all());
        return successResponse($caring);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Caring $caring)
    {
        $caring->delete();
        return successMessage('deleted');
    }
}
