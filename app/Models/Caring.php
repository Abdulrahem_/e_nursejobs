<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caring extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $with = ['nurse', 'caringType'];


    public function nurse()
    {
        return $this->belongsTo(Nurse::class, 'nurse_id');
    }
    public function caringType()
    {
        return $this->belongsTo(CaringType::class, 'type_id');
    }
}
