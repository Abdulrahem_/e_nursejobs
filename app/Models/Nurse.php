<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;



class Nurse extends Authenticatable
{
    use HasApiTokens, HasFactory;

    protected $guarded = [];

    protected $hidden = ['password'];


    public function patients()
    {
        return $this->belongsToMany(
            Patient::class,
            'carings',
            'nurse_id',
            'patient_id',
        );
    }

}
