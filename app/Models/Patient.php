<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;
    protected $guarded = [];

    public $with = ['carings'];

    public function carings()
    {
        return $this->hasMany(
            Caring::class,
            'patient_id'
        );
    }

}
