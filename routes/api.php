<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NurseController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\CaringTypeContrller;
use App\Http\Controllers\CaringController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum', 'type.admin'])->group(function() {
    Route::apiResource('nurse', NurseController::class);
    Route::apiResource('patient', PatientController::class);
    Route::apiResource('caring', CaringController::class);
    Route::apiResource('caring-type', CaringTypeContrller::class);
});

Route::post('search', [\App\Http\Controllers\SearchController::class, 'search']);

Route::post('nurse-login', [AuthController::class, 'nurseLogin']);
Route::post('admin-login', [AuthController::class, 'adminLogin']);

Route::delete('logout', [AuthController::class, 'logout'])->middleware('auth:api');

Route::get('nurse/my-patients', [NurseController::class, 'myPatients'])->middleware(['auth:sanctum', 'type.nurse']);

