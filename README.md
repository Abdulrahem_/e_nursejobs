# E_NurseJobs app


# Getting started
### Installation
<hr> 


- Clone this repository.
```
git clone https://gitlab.com/Abdulrahem_/e_nursejobs.git
```
- copy this command to terminal for install the composer.
```
composer install
```
- copy this command for generate <code>.env</code> file .
```
cp .env.example .env 
```
### Don't forget to create a database with the same name in <code>.env</code> file
- run this commands .
``` 
php artisan migrate 

php artisan db:seed

php artisan key:generate
```
- Start the local server.
```
php artisan serve 
```
## Now You Can Use This App 


